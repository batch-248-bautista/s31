/*
- What directive is used by Node.js in loading the modules it needs?
	require
- What Node.js module contains a method for server creation?
	http
- What is the method of the http object responsible for creating a server using Node.js?
	createServer() method
- What method of the response object allows us to set status codes and content types?
	response.writeHead()
- Where will console.log() output its contents when run in Node.js?
	git bash (command prompt)
- What property of the request object contains the address's endpoint?
	request.url
*/


const http = require("http");

//creates a variable "port" to store the port number
const port = 3000;

// Creates a variable 'server' that stores the output of the 'createServer' method
const server = http.createServer((request,response)=>{

	//console.log(request.url)//contains the URL endpoint

	// Accessing the 'greeting' route returns a message of 'Hello World'
	if (request.url == '/login'){

		response.writeHead(200,{'Content-Type':'text/plain'})
		response.end(`Welcome to login page!`)

	}
	// // Accessing the 'homepage' route returns a message of 'This is the homepage'
	// else if (request.url == '/homepage'){

	// 	response.writeHead(200,{'Content-Type':'text/plain'})
	// 	response.end(`This is the homepage!`)

	// }

	//MA
	//create an else condition that all other routes will return a message of "Page Not Available"

	// All other routes will return a message of 'Page not available'
	else {
		// Set a status code for the response - a 404 means Not Found
		response.writeHead(404,{'Content-Type':'text/plain'})
		response.end(`404 Page Not Available`)

	}
})
// Uses the "server" and "port" variables created above.
server.listen(port)

// When server is running, console will print the message:
console.log(`Server port ${port} is succesfully running!`)